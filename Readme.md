Check List candidature Legislative 2017
=======================================

Dépôt du mandataire
-------------------

*Candidat* et *Mandataire*

http://www.prefectures-regions.gouv.fr/ile-de-france/Region-et-institutions/Demarches-administratives/Elections/Elections-legislatives/Designation-d-un-mandataire-financier-pour-les-elections-legislatives-de-2017

Remplir le document (désignation d'un mandataire financier)[mandataire-form-paris.pdf].

Doit être signé par le *Candidat* et *Mandataire*.

L'envoyer à :
```
Bureau des élections, du mécénat et de la réglementation économique
5 rue Leblanc
75911 Paris cedex 15
```

Peut etre envoyé par mail à pref-elections@paris.gouv.fr pour recevoir le récépicé plus vite.

Récépissé reçu par mail, original envoyé par courrier.

Ouvrir un compte bancaire
-------------------------

*Mandataire*

cf http://www.cnccfp.fr/docs/campagne/20161027_guide_candidat_edition_2016.pdf page 18 sur 91, point 2.2.5.1

Le mandataire doit ouvrir un compte avec l'intitulé suivant :
```
M. X, mandataire financier de M. Y, candidat à l’élection législative 2017 - circonscription XXX-YY
```

- 075-10
```
    - Refus du crédit mutuel, avec courrier recu.
    - Mail de demande envoyé à la Banque de France de Paris (parisbastille.droitaucompte@banque-france.fr).
    - Appeler la Banque de France au 01 44 61 15 35.
    - Aller à la banque de France 3 bis place de la Bastille - 75004 Paris.
```

- 075-01
```
    - Refus du LCL reçu par mail.
```

Certificats d'inscription sur les listes électorales
----------------------------------------------------

*Candidat*, *Suppléant*

A demander à sa mairie de rattachement:
- Ronan OK
- Sandrine OK

Impression des documents
------------------------

*Candidat*

- [X] Donner la somme nécessaire à la section (1850€ / 1800€) : https://don.partipirate.org/project.php?id=6
- [X] Préparer le bulletin : [SVG](images/bulletin-ronan.svg) - [PDF](images/bulletin-ronan.pdf)
- [X] Préparer la circulaire :

    ```
        - [SVG-Recto](images/Circulaire_Ronan_recto.svg)
        - [SVG-Verso](images/Circulaire_Ronan_verso.svg)
        - [PDF-Recto](images/Circulaire_Ronan_recto.pdf)
        - [PDF-Verso](images/Circulaire_Ronan_verso.pdf)
    ```
- [ ] Préparer l'affiche : Affiche commune à 4 candidats type "Nantes".

Dépôt de candidature
--------------------

*Candidat* et *Suppléant*

cf http://www.prefectures-regions.gouv.fr/ile-de-france/Region-et-institutions/Demarches-administratives/Elections/Elections-legislatives/Depot-des-candidatures

La déclaration de candidature doit être déposée à la préfecture de Paris, 5 rue Leblanc – 75015 Paris - 01-82-52-44-35 ou 01-82-52-44-37

Rendez-vous pris à jeudi 18 à 13h30.

A fournir :
- [X] Original Candidature
- [X] Copie Candidature
- [X] Certificat d'inscription sur les listes électorales - candidat
- [X] ID Candidat
- [X] Original Acceptation Suppléant
- [X] Copie Acceptation Suppléant
- [X] Certificat d'inscription sur les listes électorales - suppleant
- [X] ID Supléant
- [X] Récépissé de déclaration de mandataire financier
- [X] ID Mandataire
- [X] Déclaration de rattachement à un groupement politique
- [X] Projet de bulletin de vote.
- [X] Projet de circulaire.
- [X] Formulaire d'acceptation de mise en ligne des professions de foi
- [X] Fiche de création d'identité du tiers CHORUS
- [X] Relevé d’identité bancaire
- [ ] Remplir la demande de subrogation
- [?] Coordonnées Candidature
- [X] Repartition de la propagande
- [?] Coordonnées pour la gestion de la propagande

Envoyer une copie du formulaire de candidature à la Caisse Claire.

Demander formulaire de compte de campagne
-----------------------------------------

*Mandataire*

Recevoir formulaire de compte de campagne
-----------------------------------------

*Mandataire*

Fermer le compte bancaire
-------------------------

*Mandataire*

Remplir formulaire de compte de campagne
-----------------------------------------

*Mandataire*

Dépôt des comptes de campagne
-----------------------------------------

*Mandataire*
